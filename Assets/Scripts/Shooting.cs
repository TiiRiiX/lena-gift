﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Shooting : MonoBehaviour
{
    public float Radius = 1f;
    public float Force = 1f;
    public GameObject Heart;
    public float DoneRotation = 180f;
    public float RotateSpeed = 100f;

    private bool isRotating = false;
    private Quaternion targetRotation = Quaternion.identity;
    
    // Start is called before the first frame update
    void Start()
    {
        targetRotation = Quaternion.Euler(new Vector3(0, DoneRotation, 0) );
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void NextLevel()
    {
        SceneManager.LoadScene("Second");
    }

    public void LastLevel()
    {
        SceneManager.LoadScene("Last");
    }

    public void LastDone()
    {
        foreach (var o in GameObject.FindGameObjectsWithTag("Voxel"))
        {
            var rb = o.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.AddExplosionForce(Force, o.transform.position, Radius);
        }

        isRotating = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;// && !EventSystem.current.IsPointerOverGameObject()
            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.CompareTag("Voxel"))
            {
                var position = hit.collider.gameObject.transform.position;
                var colliders = Physics.OverlapSphere(position, Radius);
                foreach (var collider in colliders)
                {
                    var rb = collider.GetComponent<Rigidbody>();
                    if (rb != null)
                    {
                        rb.isKinematic = false;
                        rb.AddExplosionForce(Force, position, Radius);
                    }
                }
            }
        }

        if (isRotating)
        {
            Heart.transform.rotation = Quaternion.Lerp(Heart.transform.rotation, targetRotation, RotateSpeed * Time.deltaTime);
        }
    }
}
